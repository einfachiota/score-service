const fetch = require('node-fetch');

module.exports = async (request, response) => {

  response.setHeader('Access-Control-Allow-Origin', '*')
  let url = process.env.DATA_URL || 'http://localhost:8080';
  console.log("score-service DATA_URL: ", url);

	const res = await fetch(url);
	const nodes = await res.json();


  nodes.forEach(function(node) {
    console.log("name", node.name);
    var points = 0

    var factor = 1
    node.commands.forEach(function(entry) {
      switch (entry.command) {
        case 'attachToTangle':
          factor = 50
          break
        case 'broadcastTransactions':
          factor = 5
          break
        case 'checkConsistency':
          factor = 5
          break
        case 'findTransactions':
          factor = 5
          break
        case 'getBalances':
          factor = 3
          break
        case 'getInclusionStates':
          factor = 3
          break
        case 'getNodeInfo':
          factor = 1
          break
        case 'getTransactionsToApprove':
          factor = 3
          break
        case 'getTrytes':
          factor = 3
          break
        case 'storeTransactions':
          factor = 20
          break
        case 'wereAddressesSpentFrom':
          factor = 5
          break
        default:
          multi = 1
      }
      points = points + entry.count * factor - entry.millis / 1000
    })

    node.points = Math.round(points)

    console.log("points after", node.points);

  })

  nodes.sort(function(a, b) {
      return  b.points - a.points;
  });

	return nodes;
}
